# README #


### What is this repository for? ###

* CatalogBundle is a bundle to add a module of catalog to a project.

### How do I get set up? ###

Just install it via composer and you can add a service in your app to check the permissions before make a upload or edit a file.

##Add it to AppKernel.php.##
new \Greetik\CatalogBundle\CatalogBundle()


##In the config.yml you can add your own service##
catalog:
    permsservice: app.catalog
    interface: AppBundle:Catalog