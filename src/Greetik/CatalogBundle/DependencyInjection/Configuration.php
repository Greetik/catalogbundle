<?php

namespace Greetik\CatalogBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('catalog');

        $rootNode
            ->children()
                ->scalarNode('permsservice')->defaultValue('catalog.tools')->end()
                ->scalarNode('interface')->defaultValue('CatalogBundle:Product')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
