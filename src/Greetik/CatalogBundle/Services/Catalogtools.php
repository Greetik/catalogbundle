<?php

namespace Greetik\CatalogBundle\Services;

use Greetik\CatalogBundle\Entity\Product;
use Greetik\CatalogBundle\Entity\Productvalue;
use Greetik\WebformsBundle\DBAL\Types\WebfieldType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Catalogtools {

    private $em;
    private $interfacetools;
    private $forms;
    private $sections;

    public function __construct($_entityManager, $_interfacetools, $_forms, $_sections) {
        $this->em = $_entityManager;
        $this->interfacetools = $_interfacetools;
        $this->forms = $_forms;
        $this->sections = $_sections;

        //for caché info
        $this->categoriesTitles = array();
        $this->fields = null;
    }

    public function getProductsByProject($project = '', $categories = array(), $orderby = '') {
        if (empty($project)) {
            if (count($categories) == 0)
                return $this->setDataProducts($this->em->getRepository('CatalogBundle:Product')->findAll());
            $products = $this->setDataProducts($this->em->getRepository('CatalogBundle:Product')->findByCategories($categories));
        }
        if (count($categories) == 0)
            $products = $this->setDataProducts($this->em->getRepository('CatalogBundle:Product')->findAll($project));

        $products = $this->setDataProducts($this->em->getRepository('CatalogBundle:Product')->findByProjectAndCategories($project, $categories));

        if (empty($orderby))
            return $products;

        
        $this->orderby = $orderby;
        usort($products, function($a, $b) {
            if ($a[$this->orderby] == $b[$this->orderby]) {
                return 0;
            }
            return ($a[$this->orderby] < $b[$this->orderby]) ? -1 : 1;
        });
        return $products;
    }

    public function getProductsByPage($project = '', $page, $productperpage = 10, $categories = array(), $orderby = '') {
        return $this->setDataProducts($this->em->getRepository('CatalogBundle:Product')->findProductsByPage($project, $page, $productperpage, $categories));
    }

    public function getProductObject($id) {
        return $this->em->getRepository('CatalogBundle:Product')->findOneById($id);
    }

    public function getProduct($id) {
        $product = $this->em->getRepository('CatalogBundle:Product')->getProduct($id);
        return $this->setDataProduct($product);
    }

    protected function setDataProductValue($value) {
        switch ($this->fields[$value['productfield']]['formfieldtype']) {
            case WebfieldType::SELECT: foreach ($this->fields[$value['productfield']]['formfieldoptions'] as $v)
                    if ($v['id'] == $value['productvalue'])
                        return $v['name'];
            case WebfieldType::MULTISELECT: $string = '';
                $value['productvalue'] = $value['productvalue'] ? unserialize($value['productvalue']) : array();
                foreach ($this->fields[$value['productfield']]['formfieldoptions'] as $v)
                    if (in_array($v['id'], $value['productvalue']))
                        $string .= (($string == '') ? '' : ', ') . $v['name'];
                return $string;
            case WebfieldType::CHECKBOX: return $value['productvalue'] ? true : false;
            case WebfieldType::DATE: return new \Datetime($value['productvalue']);
            case WebfieldType::DATETIME: return new \Datetime($value['productvalue']);
        }
        return $value['productvalue'];
    }

    protected function setDataProducts($products) {
        foreach ($products as $k => $v) {
            $products[$k] = $this->setDataProduct($v);
        }

        return $products;
    }

    protected function setDataProduct($product) {
        $product['values'] = $this->em->getRepository('CatalogBundle:Productvalue')->getProductValues($product['id']);
        if (!$this->fields) {
            foreach ($this->getCatalogForm($product['project']) as $v) {
                $this->fields[$v['id']] = $v;
            }
        }
        $values = array();
        foreach ($product['values'] as $value) {
            $values[$value['productfield']] = $value;
            $product['field_' . $value['productfield']] = $this->setDataProductValue($value);
            if (!isset($product['mainfield']))
                $product['mainfield'] = $product['field_' . $value['productfield']];
        }
        $product['values'] = $values;

        if (!isset($this->categoriesTitles[$product['category']])) {
            $this->categoriesTitles[$product['category']] = $this->sections->getTreesectionTitle($product['category']);
        }

        $product['categorytitle'] = $this->categoriesTitles[$product['category']];
        return $product;
    }

    public function modifyProduct($product, $project, $form) {
        $fields = $this->getDataProductFromForm($project, $form);
        $this->em->persist($product);

        foreach ($fields as $k => $v) {
            if ($v['value'] instanceof \DateTime) {
                $v['value'] = $v['value']->format('Y-m-d H:i:s');
            }

            $productvalue = $this->em->getRepository('CatalogBundle:Productvalue')->findOneBy(array('productfield' => $v['field'], 'product' => $product));
            if (!$productvalue)
                $productvalue = new Productvalue();

            $productvalue->setProduct($product);
            $productvalue->setProductfield($v['field']);
            $productvalue->setProductvalue($v['value']);
            $this->em->persist($productvalue);
        }

        $this->em->persist($product);
        $this->em->flush();
    }

    public function getDataProductFromForm($project, $form) {
        $fields = $this->getCatalogForm($project);
        $data = array();
        foreach ($fields as $field) {
            if ($field['formfieldtype'] == WebfieldType::MULTISELECT) {
                $value = serialize($form->get('field_' . $field['id'])->getData());
            } else
                if (is_array($form)){
                    foreach ($form as $_field){
                        if ($field['id']==$_field['field']){
                            $value = $_field['value'];
                            break;
                        }
                    }
                    
                }else $value = $form->get('field_' . $field['id'])->getData();
            $data[] = array('field' => $field['id'], 'value' => $value);
        }
        return $data;
    }

    public function insertProduct($product, $project, $form) {
        $fields = $this->getDataProductFromForm($project, $form);

        $product->setProject($project);
        $this->em->persist($product);

        foreach ($fields as $k => $v) {
            if ($v['value'] instanceof \DateTime) {
                $v['value'] = $v['value']->format('Y-m-d');
            }

            $productvalue = new Productvalue();
            $productvalue->setProduct($product);
            $productvalue->setProductfield($v['field']);
            $productvalue->setProductvalue($v['value']);
            $this->em->persist($productvalue);
        }

        $this->em->persist($product);
        $this->em->flush();
    }

    public function masivedeleteProduct($ids) {
        $data = array('num' => 0, 'warnings' => '');
        foreach ($ids as $id) {
            try {
                $this->deleteProduct($this->getProductObject($id));
                $data['num'] ++;
            } catch (\Exception $e) {
                $data['warnings'] .= (($data['warnings'] == '') ? '' : ', ') . 'No se pudo eliminar el elemento con id ' . $id;
            }
        }

        return $data;
    }

    public function deleteProduct($product) {
        $this->em->remove($product);
        $this->em->flush();
    }

    public function getCatalogForm($project, $onlyid = false) {
        if ($onlyid)
            return $this->getCatalogFormId($project);
        return $this->forms->getFormfieldsByProject($this->getCatalogFormId($project));
    }

    public function getCategories($project, $category = '', $onlyid = false) {
        if ($onlyid)
            return $this->getCatalogCategoriesId($project);
        return $this->sections->getTreesectionsByModule($this->getCatalogCategoriesId($project), $category);
    }

    public function getCategoriesForSelect($project) {
        return $this->sections->getTreesectionsForSelect($this->getCatalogCategoriesId($project));
    }

    public function getNumberOfProductsByProject($project) {
        return $this->em->getRepository('CatalogBundle:Product')->findNumberOfProductsByProject($project);
    }

    public function getCatalogFormId($project) {
        return $this->em->getRepository('CatalogBundle:Catalogconfig')->getCatalogForm($project);
    }

    public function getCatalogCategoriesId($project) {
        return $this->em->getRepository('CatalogBundle:Catalogconfig')->getCatalogTreesection($project);
    }

    public function getCatalogFields($project) {
        return $this->em->getRepository('CatalogBundle:Catalogconfig')->getCatalogFields($project);
    }

    public function getCatalogConfig($project) {
        return $this->em->getRepository('CatalogBundle:Catalogconfig')->findOneByProject($project);
    }

    public function saveConfig($config) {
        $formconfig = $this->forms->getFormconfig($config->getWebform());
        $formconfig->setIgnoremail(true);
        $this->forms->saveConfig($formconfig);

        $this->em->persist($config);
        $this->em->flush();
    }

    public function getProductPerm() {
        return true;
    }

    protected function importLine($line, $i, $idmodule, $fields, $totalfields) {
        if (trim($line[0]) == '')
            $inserting = true;
        else if (!is_numeric(trim($line[0])))
            return false;

        if (count($line) != $totalfields + 1)
            throw new \Exception('El número de celdas de la línea ' . $i . ' es incorrecto. Tiene ' . count($line) . ' celdas y se esperaban ' . $totalfields . '.');
        try {
            if (isset($inserting) && $inserting == true) {
                $product = new Product();
                $inserting = true;
            } else
                $product = $this->catalog->getProduct($line[0]);

            if (trim($line[1]) != '')
                $product->setSection($line[1]);
            if (trim($line[3]) != '')
                $product->setMetatitle($line[3]);
            if (trim($line[4]) != '')
                $product->setMetadescription($line[4]);
            if (trim($line[5]) != '')
                $product->setTags($line[5]);

            $data = array();
            $j = 6;
            foreach ($fields as $k => $v) {
                $data['field_' . $v['id']] = $line[$j];
                $j++;
            }

            if (isset($inserting) && $inserting == true)
                $this->insertProduct($product, $idmodule, $data);
            else
                $this->modifyProduct($product, $data);
        } catch (\Exception $e) {
            //throw $e;
            return false;
        }
        return true;
    }

    public function importcatalog($filename, $idproject) {
        $productfields = $this->getCatalogForm($idproject);
        $totalfields = 6;
        foreach ($productfields as $k => $pf) {
            if ($pf['formfieldtype'] != WebfieldType::RICHTEXT)
                $totalfields++;
            else
                unset($productfields[$k]);
        }

        try {
            $content = file_get_contents($filename);
            if (!$content)
                throw new \Exception('Ha ocurrido un error cargando el fichero, cárguelo de nuevo, por favor.');
        } catch (\Exception $e) {
            throw new \Exception('Ha ocurrido un error cargando el fichero, cárguelo de nuevo, por favor.');
        }

        if (!mb_detect_encoding($content, 'UTF-8', true))
            throw new \Exception('El fichero debe estar guardado en UTF-8');

        $data = array('numimported' => 0, 'warnings' => '');
        $lines = explode("\n", $content);

        $i = 0;
        $j = 0;
        $type = '';
        foreach ($lines as $line) {
            if ($i > 10000)
                throw new \Exception('El límite del fichero son 10000 líneas');

            $i++;
            $fields = explode(';', $line);
            if ($i <= 1)
                continue;

            if (count($fields) <= 1)
                continue;

            if (!$this->importLine($fields, $i, $idproject, $productfields, $totalfields))
                $data['warnings'] .= $data['warnings'] == "" ? "" : "\n" . "La fila " . $i . " no se pudo importar";
            else
                $data['numimported'] ++;
        }

        //$this->em->flush();

        return $data;
    }

}
