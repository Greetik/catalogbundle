<?php

namespace Greetik\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 *
 * @ORM\Table(name="product", indexes={
 *      @ORM\Index(name="project", columns={"project"}),  @ORM\Index(name="category", columns={"category"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\CatalogBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project", type="integer")
     */
    private $project;

    /**
     * @var integer
     *
     * @ORM\Column(name="category", type="integer",nullable=true)
     */
    private $category;

    /**
     *  @var integer
     *
     * @ORM\Column(name="numorder", type="integer", nullable=true)
     */
    private $numorder;


    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=255, nullable=true)
     */
    private $tags;
    
    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="metatitle", type="string", length=255, nullable=true)
     */
    private $metatitle;
    
    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="metadescription", type="string", length=255, nullable=true)
     */
    private $metadescription;

    /**
     * @ORM\OneToMany(targetEntity="Productvalue", mappedBy="product")
     */
    private $productvalues;      

    public function __construct() {
        $this->productvalues = new ArrayCollection();
    }

  


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set project
     *
     * @param integer $project
     *
     * @return Product
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return integer
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set category
     *
     * @param integer $category
     *
     * @return Product
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return integer
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set tags
     *
     * @param string $tags
     *
     * @return Product
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set metatitle
     *
     * @param string $metatitle
     *
     * @return Product
     */
    public function setMetatitle($metatitle)
    {
        $this->metatitle = $metatitle;

        return $this;
    }

    /**
     * Get metatitle
     *
     * @return string
     */
    public function getMetatitle()
    {
        return $this->metatitle;
    }

    /**
     * Set metadescription
     *
     * @param string $metadescription
     *
     * @return Product
     */
    public function setMetadescription($metadescription)
    {
        $this->metadescription = $metadescription;

        return $this;
    }

    /**
     * Get metadescription
     *
     * @return string
     */
    public function getMetadescription()
    {
        return $this->metadescription;
    }

    /**
     * Add productvalue
     *
     * @param \Greetik\CatalogBundle\Entity\Productvalue $productvalue
     *
     * @return Product
     */
    public function addProductvalue(\Greetik\CatalogBundle\Entity\Productvalue $productvalue)
    {
        $this->productvalues[] = $productvalue;

        return $this;
    }

    /**
     * Remove productvalue
     *
     * @param \Greetik\CatalogBundle\Entity\Productvalue $productvalue
     */
    public function removeProductvalue(\Greetik\CatalogBundle\Entity\Productvalue $productvalue)
    {
        $this->productvalues->removeElement($productvalue);
    }

    /**
     * Get productvalues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductvalues()
    {
        return $this->productvalues;
    }

    /**
     * Set numorder
     *
     * @param integer $numorder
     *
     * @return Product
     */
    public function setNumorder($numorder)
    {
        $this->numorder = $numorder;

        return $this;
    }

    /**
     * Get numorder
     *
     * @return integer
     */
    public function getNumorder()
    {
        return $this->numorder;
    }
}
