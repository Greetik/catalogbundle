<?php

namespace Greetik\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Catalogconfig
 *
 * @ORM\Table(name="catalogconfig")
 * @ORM\Entity(repositoryClass="Greetik\CatalogBundle\Repository\CatalogconfigRepository")
 */
class Catalogconfig
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="project", type="integer", unique=true)
     */
    private $project;

    /**
     * @var int
     *
     * @ORM\Column(name="webform", type="integer")
     */
    private $webform;

    /**
     * @var int
     *
     * @ORM\Column(name="treesection", type="integer")
     */
    private $treesection;

    /**
     * @var array
     *
     * @ORM\Column(name="productfields", type="array", nullable=true)
     */
    private $productfields;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set project
     *
     * @param integer $project
     *
     * @return Catalogconfig
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return int
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set webform
     *
     * @param integer $webform
     *
     * @return Catalogconfig
     */
    public function setWebform($webform)
    {
        $this->webform = $webform;

        return $this;
    }

    /**
     * Get webform
     *
     * @return int
     */
    public function getWebform()
    {
        return $this->webform;
    }

    /**
     * Set treesection
     *
     * @param integer $treesection
     *
     * @return Catalogconfig
     */
    public function setTreesection($treesection)
    {
        $this->treesection = $treesection;

        return $this;
    }

    /**
     * Get treesection
     *
     * @return int
     */
    public function getTreesection()
    {
        return $this->treesection;
    }

    /**
     * Set productfields
     *
     * @param array $productfields
     *
     * @return Catalogconfig
     */
    public function setProductfields($productfields)
    {
        $this->productfields = $productfields;

        return $this;
    }

    /**
     * Get productfields
     *
     * @return array
     */
    public function getProductfields()
    {
        return $this->productfields;
    }
}
