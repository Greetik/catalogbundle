<?php

namespace Greetik\CatalogBundle\Repository;

/**
 * ProductRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductRepository extends \Doctrine\ORM\EntityRepository {

    private $commondatatoreturn = "p.id, p.project, p.category, p.tags, p.metatitle, p.metadescription";

    public function findByProjectAndCategories($project, $categories = array()) {
        $query = $this->getEntityManager()
                ->createQuery('SELECT ' . $this->commondatatoreturn . ' FROM CatalogBundle:Product p WHERE 1=1' . (count($categories) ? ' AND p.category IN (:categories)' : '') . ' AND p.project=:id_project')
                ->setParameter('id_project', $project);
        if (count($categories))
            $query->setParameter('categories', $categories);

        //throw new \Exception($query->getSQL());
        return $query->getResult();
    }

    public function findAll($project = '') {
        $q = $this->getEntityManager()
                ->createQuery('SELECT ' . $this->commondatatoreturn . ' FROM CatalogBundle:Product p WHERE 1=1'
                . ((!empty($project)) ? ' AND p.project=:project' : ''));
        if (!empty($project))
            $q->setParameter('project', $project);

        return $q->getResult();
    }

    public function getProduct($id) {
        return $this->getEntityManager()
                        ->createQuery('SELECT ' . $this->commondatatoreturn . ' FROM CatalogBundle:Product p WHERE p.id=:id')
                        ->setParameter('id', $id)
                        ->getSingleResult();
    }

}
