<?php
    namespace Greetik\CatalogBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\MoneyType;
    use Symfony\Component\Form\Extension\Core\Type\EmailType;
    use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
    use Symfony\Component\Form\Extension\Core\Type\DateType;
    use Symfony\Component\Form\Extension\Core\Type\NumberType;
    use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Greetik\WebformsBundle\DBAL\Types\WebfieldType;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductType
 *
 * @author Paco
 */
class ProductType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){
        
        $categories = array();
        foreach($options['_categories'] as $s) $categories[$s->getIndentedname()] = $s->getId();
        
        $builder
            ->add('category', ChoiceType::class, array(
                'choices' => $categories,
                'required'=>true,
                'expanded'=>false,
                'multiple'=>false 
            ))
                ->add('metatitle')
                ->add('metadescription')
                ->add('tags');
        
        foreach($options['_formfields'] as $formfield){
            
            $value = '';
            if (!empty($options['_values'])){
                if (!isset($options['_values'][$formfield['id']]['productvalue'])) $value='';
                else{
                    switch($formfield['formfieldtype']){
                        case WebfieldType::CHECKBOX: $value = $options['_values'][$formfield['id']]['productvalue'] ? true:false; break;
                        case WebfieldType::MULTISELECT: $value = unserialize($options['_values'][$formfield['id']]['productvalue']); break;
                        case WebfieldType::DATE: $value = new \Datetime($options['_values'][$formfield['id']]['productvalue']); break;
                        case WebfieldType::DATETIME: $value = new \Datetime($options['_values'][$formfield['id']]['productvalue']); break;
                        default: $value = $options['_values'][$formfield['id']]['productvalue']; break;
                    }
                }
            }
            
            $arrayoptions = array('label'=>$formfield['name'],'required'=>$formfield['oblig'],'mapped'=>false);
            if ($value) $arrayoptions = array_merge( array('data'=>$value),$arrayoptions);
            
            switch($formfield['formfieldtype']){                
                case WebfieldType::TEXTAREA: $classname = TextareaType::class; break;
                case WebfieldType::RICHTEXT: $classname = TextareaType::class; break;
                case WebfieldType::EMAIL: $classname = EmailType::class; break;
                case WebfieldType::CHECKBOX: $classname = CheckboxType::class; break;
                case WebfieldType::MONEY: $classname = MoneyType::class; break;
                case WebfieldType::NUMBER: $classname = NumberType::class; break;
                case WebfieldType::SELECT: $classname = ChoiceType::class; $arrayoptions = array_merge($arrayoptions, array('attr'=>array('class'=>'select2'), 'choices'=>$this->getChoicesFromField($formfield))); break;
                case WebfieldType::MULTISELECT: $classname = ChoiceType::class; $arrayoptions = array_merge($arrayoptions, array('attr'=>array('class'=>'select2'), 'choices'=>$this->getChoicesFromField($formfield), 'multiple'=>true)); break;
                case WebfieldType::DATE: $classname = DateType::class;$arrayoptions = array_merge($arrayoptions, array('attr'=>array('class'=>'datepicker'), 'widget' => 'single_text', 'format' => 'dd/MM/yyyy')); break;
                case WebfieldType::DATETIME: $classname = DatetimeType::class; $arrayoptions = array_merge($arrayoptions, array('attr'=>array('class'=>'datetimepicker'),'widget' => 'single_text', 'format' => 'dd/MM/yyyy - HH:mm')); break;
                default: $classname = TextType::class; break;
            }
            
            $builder->add('field_'.$formfield['id'], $classname, $arrayoptions);
        }
                            
    }
    
    protected function getChoicesFromField($formfield){
        $data = array();
        foreach($options = $formfield['formfieldoptions'] as $option)
            $data[$option['name']] = $option['id']; 
        return $data;
    }
    
    public function getName(){
        return 'Product';
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\CatalogBundle\Entity\Product',
            '_formfields' => array(),
            '_categories' => array(),
            '_values' => null
        ));
    }    
}

