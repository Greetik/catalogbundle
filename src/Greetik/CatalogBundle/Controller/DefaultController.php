<?php

namespace Greetik\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\CatalogBundle\Entity\Product;
use Greetik\CatalogBundle\Form\Type\ProductType;
use Greetik\CatalogBundle\Form\Type\ViewfieldsType;
use Symfony\Component\HttpFoundation\Response;
use Greetik\CatalogBundle\Form\Type\ImportcatalogType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {
    /* Export the items to a excel */

    public function exportcatalogAction(Request $request, $idproject) {
        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv; charset="UTF-8"');
        $response->headers->set('Content-Disposition', 'attachment; filename="'.date('YmdHi').'catalogo.csv"');
        $response->headers->set('Pragma', "no-cache");
        $response->headers->set('Expires', "0");
        $response->headers->set('Content-Transfer-Encoding', "binary");

        return $this->render('CatalogBundle:Import:export.csv.twig', array(
                    'data' => $this->get($this->getParameter('catalog.permsservice'))->getProductsByProject($idproject),
                    'productfields' => $this->get($this->getParameter('catalog.permsservice'))->getCatalogForm($idproject),
                    'hostname' => $request->getSchemeAndHttpHost()
                        ), $response);
    }

    /*render form to import the catalog*/
    public function importcatalogformAction($idproject) {
        $formcsv = $this->createForm(ImportcatalogType::class);

        return $this->render('CatalogBundle:Import:index.html.twig', array(
                    'new_form' => $formcsv->createView(),
                    'idproject' => $idproject
        ));
    }    
    
    /* import the catalog from a csv */

    public function importcatalogAction(Request $request, $idproject) {

        $formcsv = $this->createForm(ImportcatalogType::class);
        $formcsv->handleRequest($request);
        
        try {
            if (count($request->files) <= 0 || !$request->files->get('importcatalog')) {
                $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error cargando fichero 1, cárguelo de nuevo, por favor ')));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }

            $data = $this->get($this->getParameter('catalog.permsservice'))->importcatalog($request->files->get('importcatalog')['csv'], $idproject);
            $response = new Response(json_encode(array('errorCode' => 0, 'data' => $data)));
        } catch (\Exception $e) {
            $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage() /*. ' (' . $e->getFile().' - '. $e->getLine() . ')'*/)));
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }    

    
    /**
     * Render all the products of the user
     * 
     * @author Pacolmg
     */
    public function indexAction($idproject) {
        $productfields = $this->get($this->getParameter('catalog.permsservice'))->getCatalogForm($idproject);
        $fieldstoview = $this->get('catalog.tools')->getCatalogFields($idproject);
        
        $productfieldstoview = array();
        foreach ($productfields as $pf){
            if (in_array($pf['id'], $fieldstoview))
                array_push($productfieldstoview, $pf);
        }
        

        return $this->render($this->getParameter('catalog.interface').':index.html.twig', array(
                    'data' => $this->get($this->getParameter('catalog.permsservice'))->getProductsByProject($idproject),
                    'idproject' => $idproject,
                    'productfields' => $productfieldstoview,
                    'insertAllow' => $this->get($this->getParameter('catalog.permsservice'))->getProductPerm('', 'insert'),
                    'formid' => $this->get($this->getParameter('catalog.permsservice'))->getCatalogForm($idproject, true),
                    'treecategoryid' => $this->get($this->getParameter('catalog.permsservice'))->getCategories($idproject, '', true),
                    'new_form' => $this->createForm(ViewfieldsType::class, null, array('_formfields'=>$productfields, '_values'=>$fieldstoview))->createView()
        ));
    }

    /**
     * View an individual product, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewAction($id, $editForm = '') {
        $product = $this->get($this->getParameter('catalog.permsservice'))->getProduct($id);
        $productfields = $this->get($this->getParameter('catalog.permsservice'))->getCatalogForm($product['project']);
        $productObject = $this->get('catalog.tools')->getProductObject($id);
        
        if (!$editForm)
            $editForm = $this->createForm(ProductType::class, $productObject, array('_formfields'=>$productfields, '_categories'=>$this->get($this->getParameter('catalog.permsservice'))->getCategoriesForSelect($product['project']), '_values'=>$product['values']));

        return $this->render($this->getParameter('catalog.interface').':view.html.twig', array(
                    'item' => $product,
                    'new_form' => $editForm->createView(),
                    'option' => '',
                    'productfields' => $productfields,
                    'modifyAllow' => $this->get($this->getParameter('catalog.permsservice'))->getProductPerm($productObject, 'modify')
        ));
    }

    
    /**
     * View an individual product, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function imagesAction($idproject) {
        
        return $this->render('CatalogBundle:Images:view.html.twig', array( 'idproject' => $idproject,
                'modifyAllow' => true
        ));
    }

    
    /**
     * Get the data of a new Product by Product and persis it
     * 
     * @param Product $item is received by Product Request
     * @author Pacolmg
     */
    public function insertAction(Request $request, $idproject) {
        $product = new Product();
        $productfields = $this->get($this->getParameter('catalog.permsservice'))->getCatalogForm($idproject);
        
        $newForm = $this->createForm(ProductType::class, $product, array('_formfields'=>$productfields, '_categories'=>$this->get($this->getParameter('catalog.permsservice'))->getCategoriesForSelect($idproject)));
        
        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try {
                   $this->get($this->getParameter('catalog.permsservice'))->insertProduct($product, $idproject, $newForm);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage().' -> '.$e->getFile().' -> '.$e->getLine());
                    return $this->render($this->getParameter('catalog.interface').':insert.html.twig', array('idproject' => $idproject, 'productfields' => $productfields, 'new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('catalog_viewcatalog', array('idproject' => $idproject)));
            }
        }

        return $this->render($this->getParameter('catalog.interface').':insert.html.twig', array('idproject' => $idproject, 'productfields' => $productfields, 'new_form' => $newForm->createView()));
    }

    /**
     * Edit the data of an product
     * 
     * @param int $id is received by Get Request
     * @param Product $item is received by Product Request
     * @author Pacolmg
     */
    public function modifyAction(Request $request, $id) {

        $product = $this->get($this->getParameter('catalog.permsservice'))->getProduct($id);
        $productObject = $this->get('catalog.tools')->getProductObject($id);

        $editForm = $this->createForm(ProductType::class, 
                $productObject, array(
            '_formfields'=>$this->get($this->getParameter('catalog.permsservice'))->getCatalogForm($product['project']), 
            '_categories'=>$this->get($this->getParameter('catalog.permsservice'))->getCategoriesForSelect($product['project'])));
                

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $this->get($this->getParameter('catalog.permsservice'))->modifyProduct($productObject, $product['project'], $editForm);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage() . ' - '.$e->getFile().' - ' .$e->getLine() );
                }
                return $this->redirect($this->generateUrl('catalog_viewproduct', array('id' => $id)));
            }
        }
        return $this->viewAction($id, $editForm);
    }

    public function deleteAction($id) {
        $product = $this->get($this->getParameter('catalog.permsservice'))->getProduct($id);
        $productObject = $this->get('catalog.tools')->getProductObject($id);
            
        try{
            if (!$product) throw new \Exception('No se encontró el producto');
            $this->get($this->getParameter('catalog.permsservice'))->deleteProduct($productObject);
            $this->addFlash('success', 'Eliminado con éxito');
        }catch(\Exception $e){
            $this->addFlash('error', $e->getMessage());
        }            
        return $this->redirect($this->generateUrl('catalog_viewcatalog', array('idproject' => $product['project'])));
    }
    
    public function masivedeleteAction(Request $request, $idproject) {
        $ids = $request->get('ids');
                
        try{
            $data = $this->get($this->getParameter('catalog.permsservice'))->masivedeleteProduct($ids);
            return new Response(json_encode(array('errorCode' => 0, 'warning'=>$data['warnings'],'data' => $data)), 200, array('Content-Type' => 'application/json'));
        }catch(\Exception $e){
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage().' - '.$e->getLine().' - '.$e->getFile() )), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error Desconocido' )), 200, array('Content-Type' => 'application/json'));
    }

    public function modifyfieldstoviewAction(Request $request, $idproject) {

        $editForm = $this->createForm(ViewfieldsType::class, null, array('_formfields'=>$this->get($this->getParameter('catalog.permsservice'))->getCatalogForm($idproject)));

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $config = $this->get($this->getParameter('catalog.permsservice'))->getCatalogConfig($idproject);
                    $config->setProductfields($editForm->get('fieldstoview')->getData());
                    $this->get($this->getParameter('catalog.permsservice'))->saveConfig($config);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        return $this->redirect($this->generateUrl('catalog_viewcatalog', array('idproject' => $idproject)));
    }

}
